# SnapKit

This is a development version of remastersys rebranded to _SnapKit_.
_SnapKit_ will take a snapshot of your installed system and create a bootable CD/DVD.

A CLI Installer is included, so the CD/DVD can be distributed.

A GUI Installer is in the process of being prepared, see [the SnapKit-Live-Installer](https://gitlab.com/HubSharkOS/SnapKit-Live-Installer)

I hope it will work well.

 >>>---> low level of development <---<<<


## License Info

Original License: GNU GPL v2

Our License: GNU GPL v3


## Version(s)

Current Status: 3.0.1


Starting Base Version: 3.0.0-1


## Requirenments

_requires_ is layed out for systemD (Debian 8/9)

_requires-dev1_ is done for sysV (Devuan for example (or Debian below 8))
